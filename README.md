# productivity-app
### This is a simple app that dynamically creates a list of user inputs as to-do items. 

### To use: 
* Enter the task you want to complete.
* When completed, click on the box with a "✔" and the item will be removed. The productivity score will also go up by one.
* Click the clear button to start fresh.

### The tools used include: HTML, CSS, Javascript, jQuery, and Bootstrap. LocalStorage is used to save and display user input even when the tab or browser is closed.

### The localStorage is cleared when the "Clear All" button is clicked. 

### Points are incremented for each completed task to encourage the user to be productive. The app is also fully mobile responsive.

#### Deployed at https://zoe-gonzales.github.io/productivity-app/

#### Screenshots

#### Start page
![start](assets/images/start.png)

#### Adding a task
![add](assets/images/add.png)

#### Completing a task
![complete](assets/images/complete.png)

#### Portrait on iPhone 5
![portrait](assets/images/portrait.png)

#### Landscape on iPhone 5 
![landscape](assets/images/landscape.png)
