// Function populates list
function buildToDoList(){
    // Empties div with id of to-do to prevent repeats
    $("#to-dos").empty();
    // Loops through current list (if items exist in localStorage)
    for (var i=0; i < toDoList.length; i++){
        // Creates pag tag to hold task
        var task = $("<p>");
        task.text(toDoList[i]);
        // Creating id to select p later
        task.attr("id", `task-${i}`);
        task.addClass("to-do-task")
        // Adding button to complete
        var completeButton = $("<button>");
        completeButton.text("✔");
        // Data-attribute holds index of items so that array can be manipulated later on
        completeButton.attr("data-task", i);
        completeButton.addClass("complete-task btn btn-primary");
        // Attaching button to p and p to larger div
        task.prepend(completeButton);
        $("#to-dos").append(task);
    }
}

// Function clears score, #to-dos div, and localStorage
function clearAll(){
    // Emptying div
    $("#to-dos").empty();
    // Clearing localStorage
    localStorage.clear();
    // Resets score to 0
    score = 0;
    // Shows score on page
    $("#score").text(score);
}

// Event listener on #add-task
$("#add-task").on("click", function(event){
    // preventing form from submitting (default action)
    event.preventDefault();
    // Retrieves value of input
    var toDoItem = $("#task").val().trim();
    // Adds value of input to the toDoItem array
    toDoList.push(toDoItem);
    // Repopulates list
    buildToDoList(toDoList);
    // Clear input field
    $("#task").val("");
    // Sets localStorage to hold updated toDoList array
    localStorage.setItem("toDo", JSON.stringify(toDoList));
});

// Event listener on each to-do-list item
$(document).on("click", ".complete-task", function(){
    // Retrieves value of "data-task" attr of clicked button
    var toDoNum = $(this).attr("data-task");
    // Removes element from DOM
    $(`#task-${toDoNum}`).remove();
    score++;
    $("#score").text(score);
    // Removes items from toDoList array
    toDoList.splice(toDoNum, 1);
    localStorage.setItem("toDo", JSON.stringify(toDoList));
    localStorage.setItem("score", JSON.stringify(score));
    // Sets localStorage to hold updated toDoList array
    buildToDoList(toDoList);
});

// Click even for #clear button
$("#clear").on("click", clearAll);

// Retrieves localStorage data upon page load
var toDoList = JSON.parse(localStorage.getItem("toDo"));
var score = localStorage.getItem("score", parseInt(score));
$("#score").text(score);

// Check if items in localStorage are in array
if (!Array.isArray(toDoList)) {
    toDoList = [];
    score = 0;
}

buildToDoList(toDoList);